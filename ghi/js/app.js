function createCard(name, description, pictureUrl, start, end, location) {
	return `
    <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">${new Date(
					start
				).toLocaleDateString()} - ${new Date(end).toLocaleDateString()}</div>
    </div>
    `;
}

function triggerAlert() {
	alert("An error occurred");
}

window.addEventListener("DOMContentLoaded", async () => {
	const url = "http://localhost:8000/api/conferences/";

	try {
		const response = await fetch(url);

		if (!response.ok) {
			triggerAlert();
			// Figure out what to do when the response is bad
		} else {
			const data = await response.json();
			let count = 0;

			for (let conference of data.conferences) {
				const detailUrl = `http://localhost:8000${conference.href}`;
				const detailResponse = await fetch(detailUrl);
				if (detailResponse.ok) {
					const details = await detailResponse.json();
					const name = details.conference.name;
					const description = details.conference.description;
					const pictureUrl = details.conference.location.picture_url;
					const starts = details.conference.starts;
					const ends = details.conference.ends;
					const location = details.conference.location.name;
					const html = createCard(
						name,
						description,
						pictureUrl,
						starts,
						ends,
						location
					);
					const column = document.querySelector(`#col-${count % 3}`);
					count++;
					column.innerHTML += html;
				}
			}
		}
	} catch (e) {
		// Figure out what to do if an error is raised
	}
});
