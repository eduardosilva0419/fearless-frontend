import React, { useEffect, useState } from "react";

function ConferenceForm(props) {
	const [locations, setLocations] = useState([]);
	// the useState is like a storage container that you store data in to be used on the page. This is normally referred to in a plural form BC it's a list referenced in the fetch argument below. It is pulling a set-list of available options which the user selects from in the Location(singular) input below.
	const [name, setName] = useState("");
	const [starts, setStarts] = useState("");
	const [ends, setEnds] = useState("");
	const [description, setDescription] = useState("");
	const [maxPresentations, setMaxPresentations] = useState("");
	const [maxAttendees, setMaxAttendees] = useState("");
	const [location, setLocation] = useState("");
	// you can only map through a list, not a empty string.

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.name = name;
		data.starts = starts;
		data.ends = ends;
		data.description = description;
		data.max_presentations = maxPresentations;
		data.max_attendees = maxAttendees;
		data.location = location;

		console.log(data);

		const conferencesUrl = "http://localhost:8000/api/conferences/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(conferencesUrl, fetchConfig);
		if (response.ok) {
			const newConference = await response.json();
			console.log(newConference);

			setName("");
			setStarts("");
			setEnds("");
			setDescription("");
			setMaxPresentations("");
			setMaxAttendees("");
			setLocation("");
		}
	};

	const fetchData = async () => {
		const url = "http://localhost:8000/api/locations/";

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setLocations(data.locations);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	const handleNameChange = (event) => {
		const value = event.target.value;
		setName(value);
	};

	const handleStartsChange = (event) => {
		const value = event.target.value;
		setStarts(value);
	};

	const handleEndsChange = (event) => {
		const value = event.target.value;
		setEnds(value);
	};

	const handleDescriptionChange = (event) => {
		const value = event.target.value;
		setDescription(value);
	};

	const handleMaxPresentationsChange = (event) => {
		const value = event.target.value;
		setMaxPresentations(value);
	};

	const handleMaxAttendeesChange = (event) => {
		const value = event.target.value;
		setMaxAttendees(value);
	};

	const handleLocationChange = (event) => {
		const value = event.target.value;
		setLocation(value);
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new conference</h1>
					<form onSubmit={handleSubmit} id="create-location-form">
						<div className="form-floating mb-3">
							<input
								value={name}
								onChange={handleNameChange}
								placeholder="Name"
								required
								type="text"
								name="name"
								id="name"
								className="form-control"
							/>
							<label htmlFor="name">Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={starts}
								onChange={handleStartsChange}
								placeholder="Starts"
								required
								type="date"
								name="starts"
								id="starts"
								className="form-control"
							/>
							<label htmlFor="starts">Starts</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={ends}
								onChange={handleEndsChange}
								placeholder="Ends"
								required
								type="date"
								name="ends"
								id="ends"
								className="form-control"
							/>
							<label htmlFor="Ends">Ends</label>
						</div>
						<div className="form-control mb-3">
							<textarea
								value={description}
								onChange={handleDescriptionChange}
								name="description"
								id="description"
								rows="10"
								className="form-control"
								placeholder="Description"
								required="required"
							></textarea>
							<label htmlFor="description">Description</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={maxPresentations}
								onChange={handleMaxPresentationsChange}
								type="number"
								id="max_presentations"
								placeholder="Max presentations"
								name="max_presentations"
								required
								className="form-control"
							/>
							<label htmlFor="max_presentations">Max presentations</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={maxAttendees}
								onChange={handleMaxAttendeesChange}
								type="number"
								id="max_attendees"
								placeholder="Max attendees"
								name="max_attendees"
								required
								className="form-control"
							/>
							<label htmlFor="max_attendees">Max attendees</label>
						</div>
						<div className="mb-3">
							<select
								value={location}
								onChange={handleLocationChange}
								required
								name="location"
								id="state"
								className="form-select"
							>
								<option value="">Location</option>
								{locations.map((location) => {
									return (
										<option key={location.id} value={location.id}>
											{location.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default ConferenceForm;
